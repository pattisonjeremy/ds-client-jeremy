import org.apache.commons.cli.*;

public class Config {

    private String advertisedHostname;
    private int connectionIntervalLimit;
    private int exchangeInterval;
    private int port;
    private String secret;
    private boolean debug;

    private static String defaultAdvertisedHostname = "localhost";
    private static int defaultConnectionIntervalLimit = 1;
    private static int defaultExchangeInterval = 600;
    private static int defaultPort = 3780;
    private static String defaultSecret = "iwqeunoeinjqweiuyweqo9cbioqwenopvewq";
    private boolean defaultDebug = false;

    Config() {
        advertisedHostname = defaultAdvertisedHostname;
        connectionIntervalLimit = defaultConnectionIntervalLimit;
        exchangeInterval = defaultExchangeInterval;
        port = defaultPort;
        secret = defaultSecret;
        debug = defaultDebug;
    }

    Config(String[] args) throws ParseException {
        Options options = getOptions();
        CommandLineParser parser = new DefaultParser();
        CommandLine cmd = parser.parse(options, args);

        advertisedHostname = cmd.hasOption("advertisedhostname") ?
            cmd.getOptionValue("advertisedHostname") :
            defaultAdvertisedHostname;

        connectionIntervalLimit = cmd.hasOption("connectionintervallimit") ?
            Integer.parseInt(cmd.getOptionValue("connectionintervallimit")) :
            defaultConnectionIntervalLimit;

        exchangeInterval = cmd.hasOption("exchangeinterval") ?
            Integer.parseInt(cmd.getOptionValue("exchangeinterval")) :
            defaultExchangeInterval;

        port = cmd.hasOption("port") ?
            Integer.parseInt(cmd.getOptionValue("port")) :
            defaultPort;

        secret = cmd.hasOption("secret") ?
            cmd.getOptionValue("secret") :
            defaultSecret;

        debug = cmd.hasOption("debug");
    }

    public static Options getOptions() {
        Options options = new Options();
        options.addOption("advertisedhostname", true, "advertised hostname");
        options.addOption("connectionintervallimit", true, "connection interval limit in seconds");
        options.addOption("exchangeinterval", true, "exchange interval in seconds");
        options.addOption("port", true, "server port, an integer");
        options.addOption("secret", true, "secret");
        options.addOption("debug", false, "print debug information");
        return options;
    }
}
