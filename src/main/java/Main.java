import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.ParseException;

import java.net.Inet4Address;

public class Main {
    public static void main(String[] args) {
        Config config;
        try {
            config = new Config(args);
        } catch (ParseException e) {
            System.out.println(e.getMessage());
            HelpFormatter helpFormatter = new HelpFormatter();
            helpFormatter.printHelp("ds-client", Config.getOptions());
            return;
        }
    }
}
